package com.indracompany.selecaojava.utils;

import com.indracompany.selecaojava.domains.Fuel;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FuelMapper implements FieldSetMapper<Fuel> {

    @Override
    public Fuel mapFieldSet(FieldSet fieldSet){
        Fuel fuel = new Fuel();
        fuel.setRegion(fieldSet.readString("region"));
        fuel.setState(fieldSet.readString("state"));
        fuel.setCity(fieldSet.readString("city"));
        fuel.setReseller(fieldSet.readString("reseller"));
        fuel.setCode(fieldSet.readString("code"));
        fuel.setProduct(fieldSet.readString("product"));

        String dt = fieldSet.readString("collectDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        if (dt.length() == 10) {
            fuel.setCollectDate(LocalDate.parse(dt, formatter));
        }

        String bp = fieldSet.readString("buyPrice").replace(",", ".");

        if (!bp.isEmpty() && bp.contains(".")) {
            fuel.setBuyPrice(Double.parseDouble(bp));
        }

        String sp = fieldSet.readString("sellPrice").replace(",", ".");

        if (!sp.isEmpty() && sp.contains(".")) {
            fuel.setSellPrice(Double.parseDouble(sp));
        }

        fuel.setMeasure(fieldSet.readString("measure"));
        fuel.setFlag(fieldSet.readString("flag"));
        return fuel;
    }
}
