package com.indracompany.selecaojava.utils;

import com.indracompany.selecaojava.domains.Fuel;
import com.indracompany.selecaojava.repositories.FuelRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class FuelWriter implements ItemWriter<Fuel> {

    @Autowired
    private FuelRepository fuelRepository;

    @Override
    public void write(List<? extends Fuel> fuel) throws Exception {
        fuelRepository.saveAll(fuel);
    }
}
