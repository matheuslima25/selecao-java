package com.indracompany.selecaojava.utils;

import com.indracompany.selecaojava.domains.Fuel;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class FuelProcessor implements ItemProcessor<Fuel, Fuel> {

    @Override
    public Fuel process(Fuel fuel) throws Exception {
        return fuel;
    }

}
