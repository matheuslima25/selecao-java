package com.indracompany.selecaojava.configurations;

import com.indracompany.selecaojava.domains.Fuel;
import com.indracompany.selecaojava.utils.FuelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
public class FuelBatchConfiguration {

    // Fuel is entity class which we have taken for the use case
    @Bean
    public Job job(JobBuilderFactory jobBuilderFactory,
                   StepBuilderFactory stepBuilderFactory,
                   ItemReader<Fuel> itemReader,
                   ItemProcessor<Fuel, Fuel> itemProcessor,
                   ItemWriter<Fuel> itemWriter
    ) {

        Step step = stepBuilderFactory.get("step")
                .<Fuel, Fuel>chunk(100)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();


        return jobBuilderFactory.get("Incrementer")
                .incrementer(new RunIdIncrementer())
                .start(step)
                .build();
    }

    @Bean
    public FlatFileItemReader<Fuel> itemReader() {
        FlatFileItemReader<Fuel> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(new ClassPathResource("2018-1_CA.csv"));
        flatFileItemReader.setName("CSV-Reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(lineMapper());
        return flatFileItemReader;
    }

    @Bean
    public LineMapper<Fuel> lineMapper() {

        DefaultLineMapper<Fuel> defaultLineMapper = new DefaultLineMapper<Fuel>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter("  ");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("region", "state", "city", "reseller", "code", "product", "collectDate", "buyPrice", "sellPrice", "measure", "flag");

        defaultLineMapper.setLineTokenizer(lineTokenizer);
        defaultLineMapper.setFieldSetMapper(new FuelMapper());

        return defaultLineMapper;
    }

}
