package com.indracompany.selecaojava.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Fuel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String region;
    private String state;
    private String city;
    private String reseller;
    private String code;
    private String product;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate collectDate;
    private Double buyPrice;
    private Double sellPrice;
    private String measure;
    private String flag;
}
