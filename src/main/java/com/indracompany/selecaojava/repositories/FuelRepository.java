package com.indracompany.selecaojava.repositories;

import com.indracompany.selecaojava.domains.Fuel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FuelRepository extends JpaRepository<Fuel, Long> {

    List<Fuel> findByOrderByReseller();
    List<Fuel> findByOrderByCollectDate();

    @Query(nativeQuery = true, value = "SELECT F.CITY, AVG(F.SELL_PRICE) FROM FUEL F GROUP BY F.CITY")
    List<Object> findAVGByCity();

    @Query(value = "SELECT f FROM Fuel f WHERE f.region= :region ")
    List<Fuel> findByRegion(@Param("region") String region);

    @Query(nativeQuery = true, value = "SELECT F.CITY, AVG(F.SELL_PRICE), AVG(F.BUY_PRICE) FROM FUEL F GROUP BY F.CITY")
    List<Object> findAVGSellandBuyByCity();

    @Query(nativeQuery = true, value = "SELECT F.FLAG, AVG(F.SELL_PRICE), AVG(F.BUY_PRICE) FROM FUEL F GROUP BY F.FLAG")
    List<Object> findAVGSellandBuyByFlag();
}
