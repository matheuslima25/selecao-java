package com.indracompany.selecaojava.controllers;

import com.indracompany.selecaojava.domains.User;
import com.indracompany.selecaojava.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder pe;

    @GetMapping
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Find User method
     *
     * @param id
     * @return User
     */
    @GetMapping(path = {"/{id}"})
    public ResponseEntity<User> findById(@PathVariable long id) {
        return userRepository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * User create method
     *
     * @param user
     */
    @PostMapping
    public User create(@RequestBody User user) {
        return userRepository.save(user);
    }

    /**
     * User update method
     *
     * @param id
     */
    @PutMapping(value="/{id}")
    public ResponseEntity<User> update(@PathVariable("id") long id, @RequestBody User user) {
        return userRepository.findById(id)
                .map(record -> {
                    record.setUsername(user.getUsername());
                    record.setPassword(pe.encode(user.getPassword()));
                    User updated = userRepository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    /**
     * User remove method
     *
     * @param id
     */
    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return userRepository.findById(id)
                .map(record -> {
                    userRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }


}

