package com.indracompany.selecaojava.controllers;

import com.indracompany.selecaojava.domains.Fuel;
import com.indracompany.selecaojava.repositories.FuelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/fuel")
public class FuelController {

    @Autowired
    private FuelRepository fuelRepository;

    /**
     * List all gas station method
     *
     * @return List<Fuel>
     */
    @GetMapping
    public List<Fuel> findAll() {
        return fuelRepository.findAll();
    }

    /**
     * List all gas station method grouped by region
     *
     * @return List<Fuel>
     */
    @GetMapping(path = "/by-region/{region}")
    public List<Fuel> groupByRegion(@PathVariable("region") String region) {
        try {
            return fuelRepository.findByRegion(region);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e.getCause());
        }
    }

    /**
     * List of avg of sell price grouped by city
     *
     * @return List<Object>
     */
    @GetMapping(path = "/avg-city")
    public List<Object> findAVGByCity() {
        try {
            return fuelRepository.findAVGByCity();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e.getCause());
        }
    }

    /**
     * List of avg of sell and buy prices grouped by city
     *
     * @return List<Object>
     */
    @GetMapping(path = "/avg-prices-cities")
    public List<Object> findAVGBuySellCity() {
        try {
            return fuelRepository.findAVGSellandBuyByCity();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e.getCause());
        }
    }

    /**
     * List of avg of sell and buy prices grouped by flag
     *
     * @return List<Object>
     */
    @GetMapping(path = "/avg-prices-flag")
    public List<Object> findAVGBuySellFlag() {
        try {
            return fuelRepository.findAVGSellandBuyByFlag();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e.getCause());
        }
    }

    /**
     * List all gas station method grouped by reseller
     *
     * @return List<Fuel>
     */
    @GetMapping(path = "/by-reseller")
    public List<Fuel> groupByReseller() {
        return fuelRepository.findByOrderByReseller();
    }

    /**
     * List all gas station method grouped by collect date
     *
     * @return List<Fuel>
     */
    @GetMapping(path = "/by-date")
    public List<Fuel> groupByCollectDate() {
        return fuelRepository.findByOrderByCollectDate();
    }

    /**
     * Find gas station method
     *
     * @param id
     * @return Fuel
     */
    @GetMapping(path = {"/{id}"})
    public ResponseEntity<Fuel> findById(@PathVariable long id) {
        return fuelRepository.findById(id)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * Fuel create method
     *
     * @param fuel
     */
    @PostMapping
    public Fuel create(@RequestBody Fuel fuel) {
        return fuelRepository.save(fuel);
    }

    /**
     * Fuel update method
     *
     * @param id
     */
    @PutMapping(value="/{id}")
    public ResponseEntity<Fuel> update(@PathVariable("id") long id, @RequestBody Fuel fuel) {
        return fuelRepository.findById(id)
                .map(record -> {
                    record.setRegion(fuel.getRegion());
                    record.setState(fuel.getState());
                    record.setCity(fuel.getCity());
                    record.setReseller(fuel.getReseller());
                    record.setCode(fuel.getCode());
                    record.setProduct(fuel.getProduct());
                    record.setCollectDate(fuel.getCollectDate());
                    record.setBuyPrice(fuel.getBuyPrice());
                    record.setSellPrice(fuel.getSellPrice());
                    record.setMeasure(fuel.getMeasure());
                    record.setFlag(fuel.getFlag());
                    Fuel updated = fuelRepository.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    /**
     * Fuel remove method
     *
     * @param id
     */
    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        return fuelRepository.findById(id)
                .map(record -> {
                    fuelRepository.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }
}
